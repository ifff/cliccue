with (import <nixpkgs> {});
let
  env = bundlerEnv {
    name = "cliccue-bundler-env";
    inherit ruby;
    gemfile  = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset   = ./gemset.nix;
  };
in stdenv.mkDerivation {
  name = "cliccue";
  buildInputs = [ env ];
}
