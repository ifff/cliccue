with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "cliccue";
  buildInputs = [ pkgconfig chruby ];
}
