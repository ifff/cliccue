# cliccue
`cliccue` is a tool for tagging image files (but technically, any other type of file would work).
You can specify what type of tags to give a file, and `cliccue` will create a directory for each tag
and put a symlink to that file within. 

For example, what if you found a painting of Tux the penguin online, and you wanted to give it the tags
`linux` and `art`? If the original file is stored in `~/Pictures/tux.png`, then running `clc tag ~/Pictures/tux.png linux art`  would get you the following index directory structure:

```
~/cliccue/
|
` linux/
| |
| ` tux.png -> ~/Pictures/tux.png
|
` art/
  |
  ` tux.png -> ~/Pictures/tux.png
```

By default, `cliccue` stores its savefile in `~/.cliccue_saves`. You can change the location of
the index directory and savefile by editing `~/.config/cliccue/config.json`:
```json
{
  "src" = "your/index/path/here/",
  "savefile" = "savefile/path/here",
}
```

For full documentation, run `clc help`.

