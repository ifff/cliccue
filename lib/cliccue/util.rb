require "cliccue/opts"

module Cliccue
  module Util
    # pretty-print a set by making a string
    # wherein the set elements are set off by commas
    def self.pretty_set s
      pretty_arr s.to_a
    end

    # pretty-print an array by separating the
    # elements by commas
    def self.pretty_arr a
      str = a.to_s
      str[1, str.length-2]
    end
  end
end
