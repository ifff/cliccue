require 'fileutils'
require 'cliccue/image'
require 'cliccue/opts'

module Cliccue
  module Filesys
    @source_dir = $OPTS['src']
    # add a tag by creating a
    # directory for it, if there isn't
    # on that exists already
    def self.add_tag tag
      # create the source directory
      # if it doesn't exist
      unless Dir.exist? @source_dir
        Dir.mkdir @source_dir
      end
      
      path = full_path tag
      unless Dir.exist? path
        Dir.mkdir path
      end
    end

    # change the directory that cliccue
    # will file any new things under
    def self.change_source_dir dir
      @source_dir = dir
    end

    # switch all the tags to the new directory
    def self.migrate_source_dir dir
      # save the previous source dir,
      # and then change it to the new one
      prev_source = @source_dir
      @source_dir = dir
      # go through the tags in the old directory,
      # and move them to the new directory
      Dir.glob(File.join prev_source, "*/").each do |tag|
        old_path = prev_source + tag
        new_path = full_path tag

        FileUtils.mv old_path, new_path
      end

      delete_if_empty prev_source
    end

    # remove empty tag directories, and return the names
    # of the tags that got removed
    def self.cleanup
      removed_tags = Array.new
      Dir.glob(File.join @source_dir, "*/").each do |path|
        if delete_if_empty path
          removed_tags << File.basename(path)
        end
      end

      removed_tags
    end

    def self.delete_tags tags
      tags.each do |tag|
        path = File.join @source_dir, tag
        FileUtils.rm_rf(path) if Dir.exist? path
      end
    end

    # add an image to the filesystem by
    # putting a symlink to it under every
    # directory that corresponds to one of
    # its tags
    def self.add_image img
      # add all the tags to the filesystem
      img.tags.each do |tag|
        add_tag tag
        symlink img.path, tag
      end
    end

    # remove an image from the given paths, and return
    # the tags that the image got removed from
    def self.remove_tags img, tags
      removed = Array.new
      basename = File.basename img.path
      # delete the image from each tag folder
      tags.each do |tag|
        link_path = File.join full_path(tag), basename
        if File.exist? link_path
          File.delete link_path
          removed << tag
        end
      end

      removed
    end
    
    # delete every link to the given image
    # from all the tag folders
    def self.delete_img img
      self.remove_tags img, img.tags
    end

    private
    # get the full path of a tag, which is
    # just the source directory + the tag
    def self.full_path tag
      File.join @source_dir, tag
    end

    # create a symlink to an image inside the
    # directory with the provided tag name
    def self.symlink full_path, tag
      basename = File.basename full_path
      link_path = File.join full_path(tag), basename
      FileUtils.ln_sf full_path, link_path
    end

    # delete a directory if it's empty, and return
    # `true` if it got deleted
    def self.delete_if_empty path
      if Dir.exist?(path) && Dir.empty?(path)
        Dir.delete path
        return true
      end
    end
  end
end
