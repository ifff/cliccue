require 'json'

module Cliccue
  # initialize the global config options,
  # with some default values included
  $OPTS = {'src' => "#{ENV['HOME']}/cliccue",
           'savefile' => "#{ENV['HOME']}/.cliccue_saves"}

  # reads a JSON config file and sets
  # $OPTS values to those in the config
  def self.read_config path
    s = File.read path
    conf = JSON.parse s
    conf.each do |k, v|
      $OPTS[k] = v
    end
  end
end
