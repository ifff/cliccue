require 'set'
require 'cliccue/util'

module Cliccue
  class Image
    attr_accessor :tags, :path, :last_update

    def initialize rel_path
      @path = File.absolute_path rel_path
      @tags = Set.new
      @last_update = Time.now
    end
    
    def add_tags tags
      @tags.merge tags
    end
    
    def delete_tags tags
      @tags -= Set.new(tags)
    end

    def clear_tags
      @tags.clear
    end
  end
end
