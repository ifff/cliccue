require 'fileutils'
require 'cliccue/image'
require 'cliccue/opts'

module Cliccue
  class Storage
    def initialize
      # create the savefile if it doesn't exist
      @savefile_path = $OPTS['savefile']
      ensure_savefile
      # a hash map mapping absolute paths of images
      # to the objects that represent them
      @images = load_images
    end

    # save the images in a file
    def save
      data = Marshal.dump @images
      write_to_savefile data
    end

    # clear out the savefile
    def rinse
      write_to_savefile
    end

    # add an image
    def <<(image)
      @images[image.path] = image
    end

    # remove tags from all images
    def unexist_tags tags
      @images.each do |path, _|
        modify_image(path) do |img|
          img.delete_tags tags
        end
      end
    end

    # add tags to an image, creating it
    # if necessary
    def add_tags path, tags
      modify_image(path) do |img|
        img.add_tags tags
      end
    end

    # delete tags from an image
    def del_tags path, tags
      modify_image?(path) do |img|
        img.delete_tags tags
      end
    end

    # clear all the tags from an image
    def clear_tags path
      modify_image?(path) do |img|
        img.clear_tags
      end
    end

    # set an image's tags to the specified new ones
    def set_tags path, tags
      clear_tags path
      add_tags path
    end

    # delete images
    def delete paths
      paths.each do |path|
        @images.delete path
      end
    end

    # load an image and return nil if nonexistent,
    # looking it up by its absolute path
    def load? path
      @images[path]
    end

    # load an image and create it if nonexistent,
    # looking it up by its absolute path
    def load_or_create path
      img = load? path
      if img
        return img
      end

      img = Image.new path
      self << image
    end

    # do something with an image, but only if it exists
    def with_image? path
      img = load? path
      if img
        yield img
      end
    end

    private
    # ensure that a savefile exists
    def ensure_savefile
      unless File.file? @savefile_path
        FileUtils.touch @savefile_path
      end
    end

    # (over)write new data to the savefile
    def write_to_savefile data=""
      File.open(@savefile_path, 'w') do |f|
        f.puts data
      end
    end

    # unmarshal the image hash from the save file
    def load_images
      contents = File.read @savefile_path
      if contents.empty?
        return Hash.new
      else
        return Marshal.load(contents)
      end
    end

    # try loading an image, and modify it
    # using the provided block argument;
    # the block won't be run if the image
    # doesn't exist
    def modify_image? path
      img = load? path
      if img
        yield img
        self << img
      end
    end

    # load or create an image, modify
    # it using a block, and then save it
    def modify_image path
      img = load_or_create path
      yield img
      self << img
    end
  end
end
