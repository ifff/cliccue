require 'thor'
require 'cliccue/filesys'
require 'cliccue/image'
require 'cliccue/version'
require 'cliccue/storage'
require 'cliccue/util'

module Cliccue
  class CliccueCLI < Thor
    class_option :quiet, :type => :boolean

    desc "tag PATH [TAGS]", "tag the file at PATH with TAGS"
    def tag path, *tags
      path = File.expand_path path
      # load/create and tag the image
      img = storage.load?(path) || Image.new(path)
      img.add_tags tags
      # add the image to the file system
      Filesys::add_image img
      # save its tags into storage
      storage << img
      # save to storage
      storage.save
      
      unless tags.empty? || options[:quiet]
        puts "added tags #{Util::pretty_arr tags} to #{path}"
      end
    end

    desc "untag PATH [TAGS]", "remove TAGS from the file at PATH; if TAGS is empty, all tags will be removed"
    def untag path, *tags
      path = File.expand_path path
      storage.with_image? path do |img|
        # if no tags were given, assume all were meant
        if tags.empty?
          tags = img.tags
        end
        # remove the tags from the filesystem and storage
        removed = Filesys::remove_tags img, tags
        storage.del_tags path, tags

        unless options[:quiet] || removed.empty?
          puts "removed tags #{Util::pretty_arr removed} from #{path}"
        end
      end

      storage.save
      
      unless !options[:quiet]
        puts "deleted tags #{Util::pretty_set tags} from #{path}"
      end
    end

    desc "unexist TAGS", "remove TAGS from the index"
    def unexist *tags
      Filesys::delete_tags tags
      storage.unexist_tags tags
      storage.save

      unless options[:quiet] || tags.empty?
        puts "deleted tags from index: #{Util::pretty_arr tags}"
      end
    end

    desc "tags PATHS", "list all the tags the files at PATHS have"
    def tags *paths
      paths = paths.map {|p| File.expand_path p}
      # go through each image and print their tags
      paths.each do |path|
        storage.with_image?(path) do |img|
          puts "tags for #{path}:" unless options[:quiet]
          img.tags.each do |tag|
            puts " * #{tag}" unless options[:quiet]
          end
        end
      end
    end

    desc "delete PATHS", "delete PATHS from the archive"
    def delete *paths
      paths = paths.map {|p| File.expand_path p}
      paths.each do |path|
        # delete the paths from the file system
        img = storage.load? path
        if img
          Filesys::delete_img img
          # delete the paths from storage
          storage.delete paths
          storage.save
          puts "deleted #{path} from tag index" unless options[:quiet]
        else
          puts "#{path} is not indexed by cliccue." unless options[:quiet]
        end
      end
    end

    desc "prune", "remove empty tag directories"
    def prune
      removed_tags = Filesys::cleanup
      unless removed_tags.empty?
        puts "removed empty tags: #{Util::pretty_arr removed_tags}" unless options[:quiet]
      end
    end

    desc "version", "output cliccue's version"
    def version
      puts VERSION unless options[:quiet]
    end
    
    private
    # a kludge that creates an instance of storage if
    # one doesn't exist, and returns it
    def storage
      if @storage.nil?
        @storage = Storage.new
      end

      @storage
    end
  end
end
